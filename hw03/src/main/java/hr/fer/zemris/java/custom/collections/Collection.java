package hr.fer.zemris.java.custom.collections;

/**
 * 
 * Interface <code>Collection/code> is a model of any kind of collection of objects.
 * 
 * @author Leo
 * @version 1.0
 * 
 */
public interface Collection {
	
	/**
	 * Method checking if a collection is empty (has no elements).
	 * 
	 * @return true if collection is empty
	 */
	default boolean isEmpty() {
		return size() == 0;
	}
	
	/**
	 * Method giving size of collection (number of objects stored in a collection).
	 * 
	 * @return size of collection
	 */
	int size();
	
	/**
	 * Method adding object to a collection.
	 * 
	 * @param value object to be added to the collection
	 */
	void add(Object value);
	
	/**
	 * Method checking if a value is contained in a collection.
	 * 
	 * @param value value to be checked upon in the collection
	 * @return true if the collection contains the given value
	 */
	boolean contains(Object value);
	
	/**
	 * Method removing the object given as input if it exists.
	 * 
	 * @param value value to remove from the collection
	 * @return true if the object was removed from the collection
	 */
	boolean remove(Object value);
	
	/**
	 * Method returning object array representation of the collection.
	 * 
	 * @return array of objects contained in the collection
	 */
	Object[] toArray();
	
	/**
	 * Method performing an operation on each collection element.
	 * 
	 * @param processor <code>Processor</code> object for processing each collection element in the way it specifies 
	 */
	default void forEach(Processor processor) {
		ElementsGetter elementsGetter = this.createElementsGetter();
		elementsGetter.processRemaining(processor);
	}
	
	
	/**
	 * Method adding all of the elements from another collection to this. This other collection remains unchanged.
	 * 
	 * @param other other collection whose elements are to be added to this one
	 */
	default void addAll(final Collection other) {
		
		/**
		 * Class <code>LocalProcessor</code> is a processor which adds an element to this collection.
		 * 
		 * @author Leo
		 * @version 1.0
		 */
		class LocalProcessor implements Processor {
			
			/**
			 * default constructor - no input
			 */
			public LocalProcessor() {
			}
			
			/**
			 * Method processing input object by adding it to this collection.
			 */
			public void process(Object value) {
				add(value);
			}
		}
		
		other.forEach(new LocalProcessor());
	}
	
	/**
	 * Method clearing a collection (removing all of its elements).
	 */
	void clear();
	
	/**
	 * Method creating getter for collection elements.
	 * @return getter for collection elements
	 */
	ElementsGetter createElementsGetter();
	
	/**
	 * Method adding all elements from another collection into this one if they fulfill the given testing condition.
	 * 
	 * @param col collection  whose elements are to be added
	 * @param tester object to test against condition
	 */
	default void addAllSatisfying(Collection col, Tester tester) {
		ElementsGetter elementsGetter = col.createElementsGetter();

		while (elementsGetter.hasNextElement()) {
			Object next = elementsGetter.getNextElement();
			if (tester.test(next)) {
				add(next);
			}
		}
	}
}
