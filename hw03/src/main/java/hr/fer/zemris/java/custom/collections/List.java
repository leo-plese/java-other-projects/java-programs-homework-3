package hr.fer.zemris.java.custom.collections;

/**
 * Interface representing general model of an element collection with basic and some additional operations.
 * 
 * @author Leo
 *
 */
public interface List extends Collection {

	/**
	 * Method getting element of a <code>List</code> at given index.
	 * 
	 * @param index index to get the element from the list from
	 * @return element at the given index in the list
	 */
	Object get(int index);

	/**
	 * Method inserting given value at given position.
	 * 
	 * @param value value to insert into the list
	 * @param position position (index) to insert the value into
	 */
	void insert(Object value, int position);

	/**
	 * Method returning the index of the first occurrence of the given value in the list.
	 * 
	 * @param value value whose index in the list is being searched for
	 * @return index of the first occurrence of the given value in the list
	 */
	int indexOf(Object value);

	/**
	 * Method removing list element from the given index in the list.
	 * 
	 * @param index index to remove the list element from
	 */
	void remove(int index);

}
