package hr.fer.zemris.java.custom.collections;

/**
 * 
 * Interface <code>Processor</code> is a model of an object enabling performing an operation an a given object.
 * 
 * @author Leo
 * @version 1.0
 * 
 */
@FunctionalInterface
public interface Processor {

	/**
	 * Method processing given object value input in some way.
	 * 
	 * @param value given object given for processing
	 */
	void process(Object value);
}
