package hr.fer.zemris.java.custom.collections.demo;

import hr.fer.zemris.java.custom.collections.Collection;
import hr.fer.zemris.java.custom.collections.ElementsGetter;
import hr.fer.zemris.java.custom.collections.LinkedListIndexedCollection;

/**
 * Class used to test <code>ElementsGetter</code> class.
 * 
 * @author Leo
 *
 */
public class ElementsGetterDemo {

	/**
	 * Main method containing tests of <code>ElementsGetter</code>.
	 * Please, feel free to uncomment one at a time to see them working.
	 * 
	 * @param args command line arguments not used in this program
	 */
	public static void main(String[] args) {
		
		//// Following can be uncommented as tests of elements getter
//		Collection col1 = new LinkedListIndexedCollection();
//		Collection col2 = new LinkedListIndexedCollection();
//		col1.add("Ivo");
//		col1.add("Ana");
//		col1.add("Jasna");
//		col2.add("Jasmina");
//		col2.add("Štefanija");
//		col2.add("Karmela");
//		ElementsGetter getter1 = col1.createElementsGetter();
//		ElementsGetter getter2 = col1.createElementsGetter();
//		ElementsGetter getter3 = col2.createElementsGetter();
//		System.out.println("Jedan element: " + getter1.getNextElement());
//		System.out.println("Jedan element: " + getter1.getNextElement());
//		System.out.println("Jedan element: " + getter2.getNextElement());
//		System.out.println("Jedan element: " + getter3.getNextElement());
//		System.out.println("Jedan element: " + getter3.getNextElement());	
		
		

//		Collection col = new ArrayIndexedCollection();
//		col.add("Ivo");
//		col.add("Ana");
//		col.add("Jasna");
//		ElementsGetter getter = col.createElementsGetter();
//		System.out.println("Jedan element: " + getter.getNextElement());
//		System.out.println("Jedan element: " + getter.getNextElement());
//		col.clear();
//		System.out.println("Jedan element: " + getter.getNextElement());

		Collection col = new LinkedListIndexedCollection();
		col.add("Ivo");
		col.add("Ana");
		col.add("Jasna");
		ElementsGetter getter = col.createElementsGetter();
		getter.getNextElement();
		// getter.getNextElement();
		// getter.getNextElement();
		// getter.getNextElement();
		getter.processRemaining(System.out::println);
		
		

	}

}
