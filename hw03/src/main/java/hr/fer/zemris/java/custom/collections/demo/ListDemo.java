package hr.fer.zemris.java.custom.collections.demo;

import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;
import hr.fer.zemris.java.custom.collections.Collection;
import hr.fer.zemris.java.custom.collections.LinkedListIndexedCollection;
import hr.fer.zemris.java.custom.collections.List;

/**
 * Class used to test <code>List</code> interface methods.
 * 
 * @author Leo
 *
 */
public class ListDemo {

	/**
	 * Main method containing test of <code>List</code>.
	 * 
	 * @param args command line arguments not used in this program
	 */
	public static void main(String[] args) {
		List col1 = new ArrayIndexedCollection();
		List col2 = new LinkedListIndexedCollection();
		col1.add("Ivana");
		col1.add("Marko");
		col1.add("Luka");
		col2.add("Jasna");
		col2.add("Neven");
		Collection col3 = col1;
		Collection col4 = col2;
		System.out.println("col1 on index 0: " + col1.get(0));
		System.out.println("col2 on index 0: " + col2.get(0));
		System.out.println("col3 on index 0: " + ((List) col3).get(0));
		System.out.println("col4 on index 0: " + ((List) col4).get(0));
		//col3.get(0); 
		//col4.get(0); 
		System.out.println("--------");
		col1.forEach(System.out::println); // Ivana Marko Luka
		System.out.println("--------");
		col2.forEach(System.out::println); // Jasna Neven
		System.out.println("--------");
		col3.forEach(System.out::println); // Ivana Marko Luka
		System.out.println("--------");
		col4.forEach(System.out::println); // Jasna Neven
	}

}
