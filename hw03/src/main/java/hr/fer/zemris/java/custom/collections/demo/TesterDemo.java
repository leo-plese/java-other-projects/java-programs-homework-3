package hr.fer.zemris.java.custom.collections.demo;

import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;
import hr.fer.zemris.java.custom.collections.Collection;
import hr.fer.zemris.java.custom.collections.LinkedListIndexedCollection;
import hr.fer.zemris.java.custom.collections.Tester;

/**
 * Class used to test <code>Tester</code> class.
 * 
 * @author Leo
 *
 */
public class TesterDemo {

	/**
	 * Main method containing test of <code>Tester</code>.
	 * Please, feel free to uncomment it to see it working.
	 * 
	 * @param args command line arguments not used in this program
	 */
	public static void main(String[] args) {
		class EvenIntegerTester implements Tester {

			@Override
			public boolean test(Object obj) {
				if (!(obj instanceof Integer)) {
					return false;
				}
				Integer i = (Integer) obj;
				return i % 2 == 0;
			}
		}

//		Tester t = new EvenIntegerTester();
//		System.out.println(t.test("Ivo")); // false
//		System.out.println(t.test(22)); // true
//		System.out.println(t.test(3)); // false
//		System.out.println(t.test(-22)); // true
//		System.out.println(t.test(-3)); // false
//		System.out.println(t.test(0)); // true

		Collection col1 = new LinkedListIndexedCollection();
		Collection col2 = new ArrayIndexedCollection();
		col1.add(2);
		col1.add(3);
		col1.add(4);
		col1.add(5);
		col1.add(6);
		col2.add(12);
		col2.addAllSatisfying(col1, new EvenIntegerTester());
		col2.forEach(System.out::println);

	}

}
