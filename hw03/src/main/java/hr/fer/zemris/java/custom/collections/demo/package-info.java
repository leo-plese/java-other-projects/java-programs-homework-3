/**
 * Package contains demo classes for testing <code>ElementsGetter</code>, <code>List</code> and <code>Tester</code> from package hr.fer.zemris.java.custom.collections.
 * 
 * @author Leo
 *
 */
package hr.fer.zemris.java.custom.collections.demo;