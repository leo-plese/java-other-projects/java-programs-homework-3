/**
 * 
 * Package contains class representations of collections modeled by <code>Collection</code> and, more specific, <code>List</code> interface some of whose implementations are e.g. <code>ArrayIndexedCollection</code> and <code>LinkedListIndexedCollection</code>.
 * They make use of <code>ElementsGetter<code> for getting their elements.
 * 
 * Alongside with these classes, there is <code>Processor</code> which enables 
 * general processing of collection data as well as <code>Tester</code> which allows for testing elements against a given condition.
 * 
 * @author Leo
 *
 */
package hr.fer.zemris.java.custom.collections;