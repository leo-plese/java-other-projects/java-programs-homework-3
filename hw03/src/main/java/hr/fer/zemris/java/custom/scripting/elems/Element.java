package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Class representing model of a data with its string representation.
 * 
 * @author Leo
 *
 */
public class Element {

	/**
	 * Method getting text representation of <code>Element</code>.
	 * @return
	 */
	public String asText() {
		return "";
	}
	
}
