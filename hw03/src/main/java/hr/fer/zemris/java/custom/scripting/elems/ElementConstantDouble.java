package hr.fer.zemris.java.custom.scripting.elems;

/**
 * 
 * Class representing model of a double constant data with its string representation.
 * 
 * @author Leo
 *
 */
public class ElementConstantDouble extends Element {
	
	/**
	 * double value representing value of constant double value
	 */
	private double value;

	/**
	 * constructor initializing to constant double value
	 * @param value constant double value
	 */
	public ElementConstantDouble(double value) {
		super();
		this.value = value;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Specifically, text representation of <code>ElementConstantDouble</code> is String representation of its constant double value.
	 */
	@Override
	public String asText() {
		return String.valueOf(value);
	}
	
	/**
	 * Method getting constant double value of <code>ElementConstantDouble</code>.
	 * 
	 * @return constant double value
	 */
	public double getValue() {
		return value;
	}
	
	

}
