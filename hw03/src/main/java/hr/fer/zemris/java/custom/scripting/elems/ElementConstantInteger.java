package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Class representing model of a integer constant data with its string representation.
 * @author Leo
 *
 */
public class ElementConstantInteger extends Element {

	/**
	 * integer value representing value of constant double value
	 */
	private int value;

	/**
	 * constructor initializing to constant integer value
	 * @param value constant integer value
	 */
	public ElementConstantInteger(int value) {
		super();
		this.value = value;
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * Specifically, text representation of <code>ElementConstantInteger</code> is String representation of its constant integer value.
	 */
	@Override
	public String asText() {
		return String.valueOf(value);
	}

	/**
	 * Method getting constant integer value of <code>ElementConstantInteger</code>.
	 * 
	 * @return constant integer value
	 */
	public int getValue() {
		return value;
	}
}
