package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Class representing model of a function with its string representation.
 * 
 * @author Leo
 *
 */
public class ElementFunction extends Element {
	
	/**
	 * name of function element
	 */
	private String name;

	/**
	 * constructor initializing function name
	 * 
	 * @param name function name
	 */
	public ElementFunction(String name) {
		super();
		this.name = name;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Specifically, text representation of <code>ElementFunction</code> is its function name String.
	 */
	@Override
	public String asText() {
		return name;
	}
	
	/**
	 * Method getting name String of <code>ElementFunction</code>.
	 * 
	 * @return function name
	 */
	public String getName() {
		return name;
	}
	
	

}
