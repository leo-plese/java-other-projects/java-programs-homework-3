package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Class representing model of a an operator on elements with its string representation.
 * 
 * @author Leo
 *
 */
public class ElementOperator extends Element {
	
	/**
	 * symbol representing operator element
	 */
	private String symbol;

	/**
	 * constructor initializing operator with its symbol
	 * 
	 * @param symbol operator symbol
	 */
	public ElementOperator(String symbol) {
		super();
		this.symbol = symbol;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Specifically, text representation of <code>ElementOperator</code> is its symbol String.
	 */
	@Override
	public String asText() {
		return symbol;
	}
	
	/**
	 * Method getting symbol String of <code>ElementOperator</code>.
	 * 
	 * @return operator symbol
	 */
	public String getSymbol() {
		return symbol;
	}
	
}
