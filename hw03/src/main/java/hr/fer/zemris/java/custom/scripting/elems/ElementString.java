package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Class representing model of a string data with its string representation.
 * 
 * @author Leo
 *
 */
public class ElementString extends Element {
	
	/**
	 * value of string element
	 */
	private String value;

	/**
	 * constructor initializing string to its String value
	 * 
	 * @param value string's String value
	 */
	public ElementString(String value) {
		super();
		this.value = value;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Specifically, text representation of <code>ElementString</code> is its value String.
	 */
	@Override
	public String asText() {
		return value;
	}

	/**
	 * Method getting symbol String of <code>ElementString</code>.
	 * 
	 * @return string String value
	 */
	public String getValue() {
		return value;
	}

}
