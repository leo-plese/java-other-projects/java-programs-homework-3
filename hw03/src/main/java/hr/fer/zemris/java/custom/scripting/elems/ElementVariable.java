package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Class representing model of a variable data with its string representation.
 * 
 * @author Leo
 *
 */
public class ElementVariable extends Element {

	/**
	 * name of variable element
	 */
	private String name;

	/**
	 * constructor initializing variable name
	 * 
	 * @param name variable name
	 */
	public ElementVariable(String name) {
		super();
		this.name = name;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Specifically, text representation of <code>ElementVariable</code> is its variable name String.
	 */
	@Override
	public String asText() {
		return name;
	}
	
	/**
	 * Method getting name String of <code>ElementVariable</code>.
	 * 
	 * @return variable name
	 */
	public String getName() {
		return name;
	}
}
