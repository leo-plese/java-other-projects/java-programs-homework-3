/**
 * Package containing <code>Element</code> and classes extending from it modeling elements of code text with the intention of being used in tokenizing/parsing process.
 * 
 * @author Leo
 *
 */
package hr.fer.zemris.java.custom.scripting.elems;