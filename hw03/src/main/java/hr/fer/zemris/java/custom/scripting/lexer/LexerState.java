package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * Enum representing possible states lexer can be found at.
 * 
 * @author Leo
 *
 */
public enum LexerState {
	/**
	 * enum value representing clear text outside of any tag
	 */
	TEXT,
	/**
	 * enum value representing for tag (non-empty tag with keyword "for"), which must be closed by an <code>END_TAG</code>
	 */
	FOR_TAG,
	/**
	 * enum value representing end tag, tag marking end of for tag
	 */
	END_TAG,
	/**
	 * enum value representing echo tag (empty tag)
	 */
	ECHO_TAG;
}
