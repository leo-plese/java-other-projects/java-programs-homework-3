package hr.fer.zemris.java.custom.scripting.lexer;

import java.util.Objects;

import hr.fer.zemris.java.custom.scripting.parser.ParseMode;

/**
 * Class representing a model of a script lexer tokenizing input string data.
 *
 * @author Leo
 */
public class SmartScriptLexer {

	/**
	 * char array containing all the characters from input String given to lexer for analysis
	 */
	private char[] data;
	/**
	 * last token produced by lexer
	 */
	private Token token;
	/**
	 * current index in <code>data</code> array
	 */
	private int currentIndex;
	/**
	 * state of lexer (enum value from this package)
	 */
	private LexerState lexerState;

	/**
	 * symbol String representations of allowed operators given as a String sequence containing thems
	 */
	private static String OPERATORS = "+-*/^";

	/**
	 * constructor initializing data array to input text String characters and initial lexer state to <code>TEXT</code> value from specified enum
	 * 
	 * @param text input text to be tokenized
	 */
	public SmartScriptLexer(String text) {
		Objects.requireNonNull(text);
		this.data = text.toCharArray();
		this.lexerState = LexerState.TEXT;
	}

	/**
	 * Method setting lexer state to one of the states from <code>LexerState</code> enum.
	 * 
	 * @param lexerState state of lexer (enum value from this package)
	 */
	public void setState(LexerState lexerState) {
		this.lexerState = Objects.requireNonNull(lexerState);
	}

	/**
	 * Method returning next token lexer produces.
	 * This method is to be called as long as there are tokens to be produced from the original text now broken down into <code>data</code> array lexer uses in tokenizing process.
	 * 
	 * @return next token produced by lexer
	 * @throws SmartScriptLexerException if any unallowed action is attempted to be performed e.g. getting one more token after all have been consumed and various cases of invalid tag expressions
	 */
	public Token nextToken() {

		if (!lexerState.equals(LexerState.TEXT)) {
			jumpOverWhitespace();
		}
		if (currentIndex != 0 && token != null && token.getValue() == null) {
			throw new SmartScriptLexerException("Last token has already been consumed!");
		}

		if (currentIndex == data.length) {
			token = new Token(TokenType.EOF, null);
			currentIndex++;
			return token;
		}

		if (lexerState.equals(LexerState.TEXT)) {
			if (data[currentIndex] == '{') {
				String tag = whichTag(currentIndex);
				if (tag.equals("")) {
					throw new SmartScriptLexerException("Invalid tag!" + ">" + tag + "<");
				}
				if ("for".equals(tag)) {
					this.setState(LexerState.FOR_TAG);

				} else if ("end".equals(tag)) {
					this.setState(LexerState.END_TAG);
				} else {
					this.setState(LexerState.ECHO_TAG);
				}

				token = new Token(TokenType.OPEN_TAG_CURLY_BRACE, "{");
				currentIndex++;
			} else {
				String txt;

				txt = getText();

				token = new Token(TokenType.TEXT, txt);

			}

			return token;
		}

		// in a tag

		if (data[currentIndex] == '}') {
			this.setState(LexerState.TEXT);
			token = new Token(TokenType.CLOSE_TAG_CURLY_BRACE, "}");
			currentIndex++;

			return token;
		} else if (data[currentIndex] == '$') {
			token = new Token(TokenType.DOLLAR_TAG_SIGN, "$");
			currentIndex++;
			return token;
		}

		if (lexerState.equals(LexerState.END_TAG)) {
			if ("end".equals(new String(data, currentIndex, 3).toLowerCase())) {
				token = new Token(TokenType.END, "end");
				currentIndex += 3;
				return token;
			} else {
				throw new SmartScriptLexerException("Invalid sequence in end tag!");
			}
		}

		if (lexerState.equals(LexerState.FOR_TAG)) {
			if ("for".equals(new String(data, currentIndex, 3).toLowerCase())) {
				token = new Token(TokenType.FOR, "for");
				currentIndex += 3;
				jumpOverWhitespace();
				int argCount = countArgumentsFor();
				if (!Character.isLetter(data[currentIndex]) || argCount < 3 || argCount > 4) {
					throw new SmartScriptLexerException("Invalid for tag!" + argCount);
				}
			} else {

				token = getNextTokenFor("for");
				if (token == null) {
					throw new SmartScriptLexerException("Invalid sequence in for tag!");
				}

			}
		} else { // ECHO_TAG
			String strTag = whichTag(currentIndex);
			if ("=".equals(strTag)) {
				token = new Token(TokenType.ECHO, strTag);
				currentIndex++;
			} else if (token.getType().equals(TokenType.DOLLAR_TAG_SIGN) && isVariable(strTag)) {
				String var = getVariable(currentIndex);
				token = new Token(TokenType.ECHO, var);

				currentIndex += var.length();
			} else {
				token = getNextTokenEcho();
				if (token == null) {
					throw new SmartScriptLexerException("Invalid sequence in echo tag!");
				}

			}
		}

		return token;
	}

	/**
	 * Method determining if the given String is a valid variable name.
	 * 
	 * @param strTag input String to be tested if it is a valid variable name
	 * @return true if the input String is a valid variable name
	 */
	private boolean isVariable(String strTag) {
		return !getVariable(currentIndex).isBlank();
	}

	/**
	 * Method getting next token from within an echo (empty) tag.
	 * First of all, it calls <code>getNextTokenFor(String)</code> method which is used to test for any case which are mutual possible elements for both for and echo tag.
	 * Then, if the input does not fall in any of the cases specified in that method, this method checks for additional cases of input such as operator and function elements.
	 * 
	 * Therefore, it also throws <code>SmartScriptLexerException</code> in case of an invalid echo tag.
	 * 
	 * @return next token from within an echo tag.
	 * @throws SmartScriptLexerException if tag is invalid (contains illegal expressions) e.g. only open but no closing brace, unsuccessful input number value formatting to appropriate numerical value
	 */
	private Token getNextTokenEcho() {
		Token token = getNextTokenFor("echo");

		if (token != null) {
			return token;
		}

		char c = data[currentIndex];
		if (c == '=') {
			token = new Token(TokenType.ECHO, "=");
			currentIndex++;
		} else if (OPERATORS.contains(Character.toString(c))) {
			token = new Token(TokenType.OPERATOR, Character.toString(c));
			currentIndex++;
		} else if (c == '@' && (currentIndex + 1) < data.length && Character.isLetter(data[currentIndex + 1])) {
			String fun = "@" + getVariable(currentIndex + 1);
			token = new Token(TokenType.FUNCTION, fun);
			currentIndex += fun.length();
		}

		return token;
	}

	/**
	 * Method getting next token from within a for (non-empty) tag.
	 * 
	 * @return next token from within a for tag.
	 * @throws SmartScriptLexerException if tag (for/echo) is invalid (contains illegal expressions) e.g. only open but no closing brace, unsuccessful input number value formatting to appropriate numerical value
	 */
	private Token getNextTokenFor(String tag) {
		char c = data[currentIndex];
		String dataStr = new String(data);
		String args = dataStr.substring(currentIndex);
		int indOfClosingBrace = args.indexOf('}');
		if (indOfClosingBrace == -1) {
			throw new SmartScriptLexerException("Invalid " + tag + " tag!");
		}
		args = args.substring(0, indOfClosingBrace);

		if (c == '"') {

			int indOfNextQuotation = args.substring(1).indexOf('"');

			if (indOfNextQuotation != -1 && indOfNextQuotation < indOfClosingBrace) {
				String builtStr = buildString(args, indOfNextQuotation);
				token = new Token(TokenType.STRING, builtStr);
				currentIndex += builtStr.length();
			}

		} else if (Character.isDigit(c)
				|| (c == '-' && (currentIndex + 1 < dataStr.length()) && Character.isDigit(data[currentIndex + 1]))) {
			String num = "";
			do {
				num += c;
				currentIndex++;
				c = data[currentIndex];
			} while (Character.isDigit(c) || c == '.');

			try {
				if (num.contains(".")) {
					Double n = Double.parseDouble(num);
					token = new Token(TokenType.DOUBLE, n);
				} else {
					Integer n = Integer.parseInt(num);
					token = new Token(TokenType.INTEGER, n);
				}
			} catch (NumberFormatException ex) {
				throw new SmartScriptLexerException("Invalid number!");
			}

		} else if (Character.isLetter(c)) {
			String var = getVariable(currentIndex);
			token = new Token(TokenType.VARIABLE, var);
			currentIndex += var.length();
		} else {
			token = null;
		}
		return token;
	}

	/**
	 * Method making a String value.
	 * Called in <code>getNextTokenFor(String)</code> to get the wanted String value.
	 * 
	 * @param args input String for building a new String from
	 * @param indOfNextQuotation index giving information about the next quotation mark in the String signaling the other mode of input String processing
	 * @return new built String from the input parameter information
	 */
	private String buildString(String args, int indOfNextQuotation) {
		String src = args.substring(0, indOfNextQuotation + 2);
		String result = "";
		char[] chars = src.toCharArray();

		int i = 0, n = chars.length;
		while (i < n) {
			if (chars[i] == '\\') {
				backslashCheck();
				result += "\\";
				i++;
				if (chars[i] == '"') {
					result += "\"";
					i++;
					indOfNextQuotation += args.substring(indOfNextQuotation + 2).indexOf('"') + 1;
					src = args.substring(0, indOfNextQuotation + 2);
					chars = src.toCharArray();
					n = chars.length;
				} else if (chars[i] == '\\') {
					result += "\\";
					i++;
				} else {
					throw new SmartScriptLexerException("Invalid escaping in string!");
				}
			} else {
				result += chars[i];
				i++;
			}
		}

		return result;
	}

	/**
	 * Method checking if the backslash is on valid index in String with aim of building a new String.
	 * 
	 * @throws SmartScriptLexerException if there is no more tokens to consume after the current one - meaning no character following the backslash
	 */
	private void backslashCheck() {
		if (currentIndex + 1 >= data.length) {
			throw new SmartScriptLexerException("There is no token to consume!");
		}
	}

	/**
	 * Method counting arguments in for tag. If the argument cannot be categorized in one of the allowed cases for for tag, the <code>SmartScriptLexerException</code> is thrown.
	 * 
	 * Valid argument to a for tag are number, variable and string values.
	 * 
	 * Method returns -1 if there is no closing brace to the for tag itself.
	 * 
	 * @return number of arguments in for tag
	 * @throws SmartScriptLexerException if the for tag contains an argument of an unallowed type
	 */
	private int countArgumentsFor() {
		String dataStr = new String(data);
		String args = dataStr.substring(currentIndex);
		int indOfClosingBrace = args.indexOf('$');
		if (indOfClosingBrace == -1) {
			return -1;
		}
		args = args.substring(0, indOfClosingBrace);

		char[] argChars = args.toCharArray();

		int argCount = 0;
		int i = 0, n = argChars.length;

		while (i < n) {
			if (Character.isWhitespace(argChars[i])) {
				i++;
				continue;
			}
			char c = argChars[i];

			if (c == '"') {	//String
				String args2 = dataStr.substring(i);

				int indOfClosingBrace2 = args2.indexOf('$');
				args2 = args2.substring(args2.indexOf('"'), indOfClosingBrace2);

				int indOfNextQuotation = args2.substring(1).indexOf('"') + 1;

				if (indOfNextQuotation != -1 && indOfNextQuotation < indOfClosingBrace2) {
					i += indOfNextQuotation + 2;
					argCount++;
				}

			} else if (Character.isDigit(c)
					|| c == '-' && (i + 1 < dataStr.length()) && Character.isDigit(argChars[i + 1])) {	//number (integer or double) value

				String num = "";
				do {
					num += c;
					i++;
					c = argChars[i];
				} while (Character.isDigit(c) || c == '.');
				argCount++;
			} else if (Character.isLetter(c)) {	//variable name
				String var = getVariable(currentIndex + i);
				i += var.length();
				argCount++;
			} else {	//other - invalid argument type
				throw new SmartScriptLexerException("Invalid argument in for tag!");
			}
		}

		return argCount;
	}

	/**
	 * Method called to get text for <code>TEXT</code> TokenType.
	 * 
	 * @return resulting text to be forwarded to <code>nextToken</code>
	 */
	private String getText() {
		String chars = "";
		int ind = currentIndex;
		int n = data.length;

		int indBrace = 0;
		if (!ParseMode.parseMode) {
			while (ind < n) {
				if (data[ind] == '{') {
					if (data[ind + 1] != '$') {
						chars += "{";
						ind++;
	
					} else {
						break;
					}
				} else if (ind + 1 < n && data[ind] == '\\' && data[ind + 1] == '{') {
	
					chars += "{";
					ind += 2;
	
				} else if (ind + 1 < n && data[ind] == '\\' && data[ind + 1] == '\\') {
					chars += "\\";
					ind += 2;
	
				} else {
					chars += data[ind];
					ind++;
				}
			}
		}

		currentIndex = indBrace != 0 ? indBrace : ind;
		return chars;
	}

	/**
	 * Method telling if the tag following from the given input index in data array is for, end or echo tag and returns appropriate String representation of its name e.g. "for", "end", and "=" or variable name
	 * for echo tag.
	 * 
	 * @param index index from which to search for the first String sequence after '$' sign marking beginning of a tag
	 * @return String representing tag name
	 */
	private String whichTag(int index) {
		int ind = index;
		ind++;
		ind = skipWhitespace(ind);

		if (data[ind] != '$') {
			return "";
		}

		if (ind + 3 >= data.length) {
			return "";
		}
		ind++;
		ind = skipWhitespace(ind);
		String tagStr = new String(data, ind, 3).toLowerCase();
		if ("for".equals(tagStr)) {
			return "for";
		} else if ("end".equals(tagStr)) {
			return "end";
		} else if ("=".equals(Character.toString(data[ind]))) {
			return "=";
		} else if (Character.isLetter(data[ind])) {
			return getVariable(ind);
		}
		return "";

	}

	/**
	 * Helper method used to skip whitespace where analyizing whitespace is unimportant.
	 * 
	 * @param ind index to start skipping whitespace from
	 * @return resulting index (after skipping all the whitespace in a row)
	 */
	private int skipWhitespace(int ind) {
		while (Character.isWhitespace(data[ind])) {
			ind++;
		}
		return ind;
	}

	/**
	 * Method getting variable name starting from given input index.
	 * 
	 * Rule: first character of variable name must be letter and others are zero or more letters/digits/underscores.
	 * 
	 * @param ind index in data array after the end of the variable name
	 * @return variable name built
	 */
	private String getVariable(int ind) {
		char c = data[ind];
		String var = "";
		while (Character.isDigit(c) || Character.isLetter(c) || c == '_') {
			var += c;
			ind++;
			c = data[ind];
		}
		return var;
	}

	/**
	 * Helper method used to jump over whitespace where analyzing whitespace is unimportant, at the same time changing <code>currentIndex</code> variable.
	 * 
	 * @param ind index to start skipping whitespace from
	 * @return resulting index (after skipping all the whitespace in a row)
	 */
	private String jumpOverWhitespace() {
		if (data.length == 0) {
			return "";
		}
		String whitespace = "";
		for (int len = data.length; currentIndex < len;) {
			if (!Character.isWhitespace(data[currentIndex])) {
				break;
			}
			whitespace += data[currentIndex];
			currentIndex++;
		}
		return whitespace;
	}
}
