package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * Class representing exception thrown by of a script lexer.
 * 
 * @author Leo
 *
 */
public class SmartScriptLexerException extends RuntimeException {

	/**
	 * version number - identifier of this class
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * default constructor calling <code>RuntimeException</code> super constructor
	 */
	public SmartScriptLexerException() {
		super();
	}

	/**
	 * constructor given information message calling <code>RuntimeException</code> super constructor forwarding message to it
	 */
	public SmartScriptLexerException(String message) {
		super(message);
	}

}
