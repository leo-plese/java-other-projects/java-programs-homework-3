package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * Enum listing all possible token types given a lexer.
 * 
 * @author Leo
 *
 */
public enum TokenType {
	/*
	 * enum value representing end of file character
	 */
	EOF, 
	/*
	 * enum value representing end character in a normal text (outside of any tag)
	 */
	TEXT,
	/*
	 * enum value representing for tag beginning
	 */
	FOR, 
	/*
	 * enum value representing end of for tag
	 */
	END, 
	/*
	 * enum value representing echo tag
	 */
	ECHO, 
	/*
	 * enum value representing variable element
	 */
	VARIABLE, 
	/*
	 * enum value representing integer element
	 */
	INTEGER, 
	/*
	 * enum value representing double element
	 */
	DOUBLE, 
	/*
	 * enum value representing String element
	 */
	STRING, 
	/*
	 * enum value representing function element
	 */
	FUNCTION, 
	/*
	 * enum value representing operator element
	 */
	OPERATOR, 
	/*
	 * enum value representing open curly brace of a tag
	 */
	OPEN_TAG_CURLY_BRACE, 
	/*
	 * enum value representing closing curly brace of a tag
	 */
	CLOSE_TAG_CURLY_BRACE, 
	/*
	 * enum value representing beginning and end of a tag marked by '$' sign
	 */
	DOLLAR_TAG_SIGN;

}
