/**
 * Package containing <code>SmartScriptLexer</code> lexer class and all the others used by the lexer such as <code>Token</code>, <code>TokenType</code> and <code>LexerState</code>.
 * 
 * @author Leo
 *
 */
package hr.fer.zemris.java.custom.scripting.lexer;