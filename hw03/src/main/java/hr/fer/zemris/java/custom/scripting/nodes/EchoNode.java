package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.elems.Element;

/**
 * 
 *  Model of an empty tag node in a document structure tree of nodes given by parser.
 *  
 * @author Leo
 *
 */
public class EchoNode extends Node {

	/**
	 * array of elements contained in echo node (strings, functions, variables, integers...)
	 */
	private Element[] elements;

	/**
	 * constructor initializing an echo node given its elements
	 * 
	 * @param elements elements contained in the echo node
	 */
	public EchoNode(Element[] elements) {
		this.elements = new Element[elements.length];
		for (int i = 0, n = elements.length; i < n; i++) {
			this.elements[i] = elements[i];
		}
	}
	
	/**
	 * Method getting an echo node's elements.
	 * 
	 * @return elements array of echo node's elements
	 */
	public Element[] getElements() {
		return elements;
	}
	
}
