package hr.fer.zemris.java.custom.scripting.nodes;

import java.util.Objects;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;

/**
 *  Model of a non-empty for tag node in a document structure tree of nodes given by parser.
 * 
 * @author Leo
 *
 */
public class ForLoopNode extends Node {
	
	/**
	 * variable element of a for node which represents a variable running from start to end index
	 */
	private ElementVariable variable;
	/**
	 * starting index element of for node
	 */
	private Element startExpression;
	/**
	 * end index element of for node
	 */
	private Element endExpression;
	/**
	 * step element of for node
	 */
	private Element stepExpression;
	
	/**
	 * constructor initializing a for node given its variable name, start and end expression and optional step expression
	 * 
	 * @param variable variable element of a for node
	 * @param startExpression starting index element of for node
	 * @param endExpression end index element of for node
	 * @param stepExpression step element of for node
	 * @throws NullPointerException if any of the three mandatory elements to a for node is <code>null</code>
	 */
	public ForLoopNode(ElementVariable variable, Element startExpression, Element endExpression,
			Element stepExpression) {
		super();
		this.variable = Objects.requireNonNull(variable);
		this.startExpression = Objects.requireNonNull(startExpression);
		this.endExpression = Objects.requireNonNull(endExpression);
		this.stepExpression = stepExpression;
	}
	
	/**
	 * Method getting variable of a for node.
	 * 
	 * @return variable of a for node
	 */
	public ElementVariable getVariable() {
		return variable;
	}
	
	/**
	 * Method getting start element of a for node.
	 * 
	 * @return start element of a for node
	 */
	public Element getStartExpression() {
		return startExpression;
	}
	
	/**
	 * Method getting end element of a for node.
	 * 
	 * @return end element of a for node
	 */
	public Element getEndExpression() {
		return endExpression;
	}
	
	/**
	 * Method getting step element of a for node.
	 * 
	 * @return step element of a for node
	 */
	public Element getStepExpression() {
		return stepExpression;
	}
	
	
}
