package hr.fer.zemris.java.custom.scripting.nodes;

import java.util.Objects;

import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;

/**
 *  Model of a general node in a document structure tree of nodes given by parser.
 * 
 * @author Leo
 *
 */
public class Node {
	
	/**
	 * collection of type <code>ArrayIndexedCollection</code> node uses for storing its children nodes
	 */
	protected ArrayIndexedCollection arrayIndexedCollection=new ArrayIndexedCollection();
	
	/**
	 * Method adding a child to the node. Allowed for non-empty tags such as for node.
	 * 
	 * @param child node to be added to this one
	 * @throws NullPointerException if given node to be added is <code>null</code>
	 */
	public void addChildNode(Node child) {
		child = Objects.requireNonNull(child);
		
		arrayIndexedCollection.add(child);
	}
	
	/**
	 * Method getting number of this node's children.
	 * 
	 * @return number of this node's children
	 */
	public int numberOfChildren() {
		return arrayIndexedCollection.size();
	}
	
	/**
	 * Method getting child of the node from the given index.
	 * 
	 * @param index index to get the child from
	 * @return node's child from specified index
	 * @throws IndexOutOfBoundsException if index invalid (index in <code>arrayIndexedCollection</code>)
	 */
	public Node getChild(int index) {
		return (Node) arrayIndexedCollection.get(index);
	}

}
