package hr.fer.zemris.java.custom.scripting.nodes;

import java.util.Objects;

/**
 *  Model of a text node in a document structure tree of nodes given by parser.
 * 
 * @author Leo
 *
 */
public class TextNode extends Node {
	
	/**
	 * text value of the text node
	 */
	private String text;

	/**
	 * constructor initializing to its text value
	 * 
	 * @param text text to initialize the node with
	 * @throws NullPointerException if given text to be added is <code>null</code>
	 */
	public TextNode(String text) {
		super();
		this.text = Objects.requireNonNull(text);
	}

	/**
	 * Method getting text value of the node.
	 * 
	 * @return text value of a text node
	 */
	public String getText() {
		return text;
	}

}
