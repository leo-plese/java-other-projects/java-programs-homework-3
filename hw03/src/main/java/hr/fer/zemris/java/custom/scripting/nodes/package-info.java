/**
 * Package containing <code>Node</code> and classes extending from it modeling nodes in tree structure parser is working with.
 * 
 * @author Leo
 *
 */
package hr.fer.zemris.java.custom.scripting.nodes;