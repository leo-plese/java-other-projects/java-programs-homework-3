package hr.fer.zemris.java.custom.scripting.parser;

/**
 * Class marking one of two possible modes of parser work - forward or backward parsing.
 * Initially set to forward parsing mode.
 * 
 * @author Leo
 *
 */
public class ParseMode {
	
	/**
	 * parse mode of a parser (initially forward-parsing mode represented by false boolean)
	 */
	public static boolean parseMode = false;

}
