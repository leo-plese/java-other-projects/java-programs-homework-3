package hr.fer.zemris.java.custom.scripting.parser;

import hr.fer.zemris.java.custom.scripting.elems.*;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptLexer;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptLexerException;
import hr.fer.zemris.java.custom.scripting.lexer.Token;
import hr.fer.zemris.java.custom.scripting.lexer.TokenType;
import hr.fer.zemris.java.custom.scripting.nodes.*;

/**
 * Model of a parser parsing input tokens input by lexer.
 * 
 * @author Leo
 *
 */
public class SmartScriptParser {

	/**
	 * top node in a tree structure parser results in
	 */
	private DocumentNode documentNode = new DocumentNode();

	/**
	 * constructor using document body to be parsed using lexer constructed from the String body
	 * 
	 * @param docBody document body to be parsed
	 * @throws SmartScriptParserException if parsing of input String was unsuccessful
	 */
	public SmartScriptParser(String docBody) {
		
		SmartScriptLexer lexer = new SmartScriptLexer(docBody);

		Token[] tokens = getTokens(lexer, docBody);
		int tokCount = 0;
		for (Token t : tokens) {
			if (t == null)
				break;
			tokCount++;
			
		}
		Token[] toksForParsing = new Token[tokCount];
		for (int i = 0; i < tokCount; i++) {
			Token t = tokens[i];
			toksForParsing[i] = t;
		}

		parse(toksForParsing);
	}

	/**
	 * Method doing the actual parsing of document based on input tokens given by lexer.
	 * It makes use of <code>ObjectStack</code> stack model for pushing and popping document and for nodes onto and from the stack structure.
	 * 
	 * @param tokens input tokens to be parsed
	 * @return resulting document node with its children nodes representing parsing tree nodes
	 * @throws SmartScriptParserException if parsing of input String was unsuccessful
	 */
	public DocumentNode parse(Token[] tokens) {
		ObjectStack objectStack = new ObjectStack();

		objectStack.push(documentNode);
	
		try {
			for (int i = 0, n = tokens.length; i < n; i++) {
				Token tok = tokens[i];
				if (tok == null) {
					
					return documentNode;
				}
				
				
				Node lastNode = (Node) objectStack.peek();
				
				TokenType type = tok.getType();
				
				if (type.equals(TokenType.TEXT)) {
					lastNode.addChildNode(new TextNode((String) tok.getValue()));
				} else if (type.equals(TokenType.OPEN_TAG_CURLY_BRACE)) {
					i++;
					Token afterOpenBraceToken = tokens[i];

					Token afterDollarSignToken = null;
					if (afterOpenBraceToken.getType().equals(TokenType.DOLLAR_TAG_SIGN)) {
						i++;
						afterDollarSignToken = tokens[i];
					}

					Token tagToken = afterDollarSignToken;
					TokenType tagTokenType = tagToken.getType();
					if (tagTokenType.equals(TokenType.FOR)) {
						i++;
						Token tokFor = tokens[i];

						ElementVariable elVar = null;
						try {
							if (tokFor.getType().equals(TokenType.VARIABLE)) {
								elVar = new ElementVariable((String) tokFor.getValue());
								i++;
							}
						} catch (Exception ex) {
							throw new SmartScriptParserException("Cannot parse first argument (variable) to for tag!");
						}

						Element[] elementsFor = new Element[3];
						int el = 0;
						tokFor = tokens[i];
						while (!tokFor.getType().equals(TokenType.DOLLAR_TAG_SIGN)) {
							TokenType tokForType = tokFor.getType();
							if (tokForType.equals(TokenType.VARIABLE)) {
								elementsFor[el] = new ElementVariable((String) tokFor.getValue());
							} else if (tokForType.equals(TokenType.STRING)) {
								elementsFor[el] = new ElementString((String) tokFor.getValue());
							} else if (tokForType.equals(TokenType.INTEGER)) {
								Integer tokInt;
								try {
									tokInt = (Integer) tokFor.getValue();
								} catch (NumberFormatException ex) {
									throw new SmartScriptParserException("Cannot parse to integer!");
								}
								elementsFor[el] = new ElementConstantInteger(tokInt);
							} else if (tokForType.equals(TokenType.DOUBLE)) {
								Double tokDouble;
								try {
									tokDouble = (Double) tokFor.getValue();
								} catch (NumberFormatException ex) {
									throw new SmartScriptParserException("Cannot parse to double!");
								}
								elementsFor[el] = new ElementConstantDouble(tokDouble);
							} else {
								throw new SmartScriptParserException("Cannot parse arguments to for tag!");
							}
							el++;
							i++;
							tokFor = tokens[i];
						}
						
						ForLoopNode forNode = new ForLoopNode(elVar, elementsFor[0], elementsFor[1], elementsFor[2]);
						lastNode.addChildNode(forNode);
						objectStack.push(forNode);
						
						lastNode = forNode;


						
					} else if (tagTokenType.equals(TokenType.ECHO)) {
						Object echoTagName = afterDollarSignToken.getValue();	
					
						i++;
						Token tokEcho = tokens[i];

						Element[] elementsEcho = new Element[1+tokens.length - i];
						int el = 0;
						elementsEcho[el] = new ElementString((String)echoTagName);
						el++;
						while (!tokEcho.getType().equals(TokenType.DOLLAR_TAG_SIGN)) {
							TokenType tokEchoType = tokEcho.getType();
							if (tokEchoType.equals(TokenType.VARIABLE)) {
								elementsEcho[el] = new ElementVariable((String) tokEcho.getValue());
							} else if (tokEchoType.equals(TokenType.STRING)) {
								elementsEcho[el] = new ElementString((String) tokEcho.getValue());
							} else if (tokEchoType.equals(TokenType.INTEGER)) {
								Integer tokInt;
								try {
									tokInt = (Integer) tokEcho.getValue();
								} catch (NumberFormatException ex) {
									throw new SmartScriptParserException("Cannot parse to integer!");
								}
								elementsEcho[el] = new ElementConstantInteger(tokInt);
							} else if (tokEchoType.equals(TokenType.DOUBLE)) {
								Double tokDouble;
								try {
									tokDouble = (Double) tokEcho.getValue();
								} catch (NumberFormatException ex) {
									throw new SmartScriptParserException("Cannot parse to double!");
								}
								elementsEcho[el] = new ElementConstantDouble(tokDouble);
							} else if (tokEchoType.equals(TokenType.FUNCTION)) {
								elementsEcho[el] = new ElementFunction((String) tokEcho.getValue());
							} else if (tokEchoType.equals(TokenType.OPERATOR)) {
								elementsEcho[el] = new ElementOperator((String) tokEcho.getValue());
							} else {
								throw new SmartScriptParserException("Cannot parse arguments to for tag!");
							}

							el++;
							i++;
							tokEcho = tokens[i];
						}
						
						
						int nonNullCount = 0;
						for (Element e : elementsEcho) {
							if (e == null) {
								break;
							}
							nonNullCount++;
						}

						Element[] elementsEchoNonNull = new Element[nonNullCount];
						for (int j = 0; j < nonNullCount; j++) {
							elementsEchoNonNull[j] = elementsEcho[j];
						}

						EchoNode echoNode =new EchoNode(elementsEchoNonNull);
						lastNode.addChildNode(echoNode);
						

						lastNode = echoNode;


					} else if (tagTokenType.equals(TokenType.END)) {
						objectStack.pop();
						if (objectStack.isEmpty()) {
							throw new SmartScriptParserException(
									"Could not parse input! Object stack was left empty after end node.");
						}

						i++;
						i++;

					} else {
						throw new SmartScriptParserException("Invalid tag name!");
					}
				} else if (type.equals(TokenType.DOLLAR_TAG_SIGN)) {
					i++;
					if (tokens[i].getType().equals(TokenType.CLOSE_TAG_CURLY_BRACE)) {
						continue;
					} else {
						throw new SmartScriptParserException("Invalid end of tag!");
					}
				} else if (type.equals(TokenType.EOF)) {
					if (objectStack.size() > 1) {
						throw new SmartScriptParserException(
								"For tag does not have closing end tag!");
					}
					
					return documentNode;
				}
			}
			
			return documentNode;
		} catch (Exception ex) {
			
			throw new SmartScriptParserException("Could not parse input! "+ex.getMessage());
		}
	}

	/**
	 * Method getting top node of a parsing tree structure.
	 * Called after parsing was finished.
	 * 
	 * @return document node - top node of a parsing tree structure
	 */
	public DocumentNode getDocumentNode() {
		return documentNode;
	}

	/**
	 * Method getting tokens from original String document body. Called in constructor to forward the input tokens to the method for parsing.
	 *  
	 * @param lexer lexer tokenizing the document body into tokens
	 * @param docBody input String to be tokenized by the given lexer
	 * @return tokens from original String document body
	 * @throws SmartScriptParserException in case lexer throws a SmartScriptLexerException in case of unsuccessful tokenizing
	 */
	private Token[] getTokens(SmartScriptLexer lexer, String docBody) {
		Token[] tokens = new Token[docBody.length() + 1];

		int i = 0;
		Token token = null;
		try {
			token = lexer.nextToken();
			while (token.getValue() != null) {
				tokens[i] = token;
				i++;
				token = lexer.nextToken();
			}
		} catch (SmartScriptLexerException ex) {
			throw new SmartScriptParserException("Could not get next token!");
		}
		tokens[i] = token;
		return tokens;
	}

}
