package hr.fer.zemris.java.custom.scripting.parser;

/**
 * Class representing exception thrown by parser in case of invalid input data.
 * 
 * @author Leo
 *
 */
public class SmartScriptParserException extends RuntimeException {

	/**
	 * identifier of the class
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * default constructor calling <code>RuntimeException</code> super constructor
	 */
	public SmartScriptParserException() {
		super();
	}


	/**
	 * constructor given information message calling <code>RuntimeException</code> super constructor forwarding message to it
	 */
	public SmartScriptParserException(String message) {
		super(message);
	}
	
}
