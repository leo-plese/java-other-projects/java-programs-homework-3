/**
 * 
 * Package containing parser <code>SmartScriptParser</code> class and other classes such as ObjectStack required for its work.
 * 
 * @author Leo
 *
 */
package hr.fer.zemris.java.custom.scripting.parser;