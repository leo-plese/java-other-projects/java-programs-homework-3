package hr.fer.zemris.java.hw03;

import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;

import hr.fer.zemris.java.custom.scripting.elems.*;
import hr.fer.zemris.java.custom.scripting.nodes.*;

/**
 * Class for testing parsing data.
 * 
 * @author Leo
 *
 */
public class SmartScriptTester {


	/**
	 * Method testing <code>SmartScriptParser</code> class given official example.
	 * 
	 * @param args command line arguments not used in this program
	 */
	public static void main(String[] args) {
		

		String docBody = "This is sample text.\r\n" + "{$ FOR i 1 10 1 $}\r\n"
				+ " This is {$= i $}-th time this message is generated.\r\n" + "{$END$}\r\n" + "{$FOR i 0 10 2 $}\r\n"
				+ " sin({$=i$}^2) = {$= i i * @sin \"0.000\" @decfmt $}\r\n" + "{$END$}";
		SmartScriptParser parser = null;
		try {
			parser = new SmartScriptParser(docBody);
		} catch (SmartScriptParserException e) {
			System.out.println("Unable to parse document!");
			System.exit(-1);
		} catch (Exception e) {
			System.out.println("If this line ever executes, you have failed this class!");
			System.exit(-1);
		}
		DocumentNode document = parser.getDocumentNode();
		String originalDocumentBody = createOriginalDocumentBody(document);
		System.out.println("ORIGINAL");
		System.out.println(originalDocumentBody); // should write something like original
		System.out.println("ORIGINALend");
		// content of docBody

		//recreating from output tree structure the original String
		SmartScriptParser parser2 = new SmartScriptParser(originalDocumentBody);
		DocumentNode document2 = parser2.getDocumentNode();
		String originalDocumentBody2 = createOriginalDocumentBody(document2);
		System.out.println("ORIGINAL_222");
		System.out.println(originalDocumentBody2); // should write something like original
		System.out.println("ORIGINALend_222");
		// now document and document2 should be structurally identical trees

	}

	/**
	 * Method creating original document body String given its tree-like structure with document node input as its top element.
	 * 
	 * @param document top element of tree structure representing node hierarchy of nodes given by parser work
	 * @return String representing original body which was parsed
	 * @throws SmartScriptParserException if a node is unexistent
	 */
	private static String createOriginalDocumentBody(DocumentNode document) {
		String original = "";
		for (int i = 0, n = document.numberOfChildren(); i < n; i++) {
			Node child = document.getChild(i);
			if (child instanceof TextNode) {
				String txt = ((TextNode) child).getText();
				original += txt;


			} else if (child instanceof EchoNode) {
				Element[] echoElems = ((EchoNode) child).getElements();
				original += "{$ ";
				for (Element el : echoElems) {
					original += el.asText() + " ";
				}
				original += " $}";

			} else if (child instanceof ForLoopNode) {
				ForLoopNode forNode = (ForLoopNode) child;
				ElementVariable var = forNode.getVariable();
				Element start = forNode.getStartExpression();
				Element end = forNode.getEndExpression();
				Element step = forNode.getStepExpression();

				original += "{$ FOR ";
				String stepStr = step == null ? "" : " " + step.asText();
				original += " " + var.asText() + " " + start.asText() + " " + end.asText() + stepStr;
				original += " $}";


				for (int j = 0, f = forNode.numberOfChildren(); j < f; j++) {

					Node childFor = forNode.getChild(j);
					if (childFor instanceof TextNode) {
						String txt = ((TextNode) childFor).getText();
						original += txt;
					} else if (childFor instanceof EchoNode) {
						Element[] echoElems = ((EchoNode) childFor).getElements();
						original += "{$";
						for (Element el : echoElems) {
							original += el.asText() + " ";
						}
						original += "$}";
					}
				}
				original += "{$END$}";
			} else {
				throw new SmartScriptParserException("Unexisting node!");
			}
		}

		return original;
	}

}