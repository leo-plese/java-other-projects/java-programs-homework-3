/**
 * 
 * Package containing tester class for the parser <code>SmartScriptParser</code> class.
 * 
 * @author Leo
 *
 */
package hr.fer.zemris.java.hw03;