package hr.fer.zemris.java.hw03.prob1;

import java.util.Objects;

/**
 * Model of a lexer tokenizing input data.
 * 
 * @author Leo
 *
 */
public class Lexer {
	
	/**
	 * char array containing all the characters from input String given to lexer for analysis
	 */
	private char[] data;
	/**
	 * last token produced by lexer
	 */
	private Token token;
	/**
	 * current index in <code>data</code> array
	 */
	private int currentIndex;
	/**
	 * state of lexer (enum value from this package)
	 */
	private LexerState lexerState;

	/**
	 * constructor initializing data array to input text String characters and initial lexer state to <code>BASIC</code> value from specified enum
	 * 
	 * @param text input text to be tokenized
	 */
	public Lexer(String text) {
		Objects.requireNonNull(text);
		data = text.toCharArray();
		lexerState = LexerState.BASIC;
	}

	/**
	 * Method setting lexer state to one of the states from <code>LexerState</code> enum.
	 * 
	 * @param lexerState state of lexer (enum value from this package)
	 */
	public void setState(LexerState lexerState) {
		this.lexerState = Objects.requireNonNull(lexerState);
	}

	/**
	 * Method returning next token lexer produces.
	 * This method is to be called as long as there are tokens to be produced from the original text now broken down into <code>data</code> array lexer uses in tokenizing process.
	 * 
	 * @return next token produced by lexer
	 * @throws LexerException if any unallowed action is attempted to be performed e.g. getting one more token after all have been consumed and various cases of invalid tag expressions
	 */
	public Token nextToken() {

		jumpOverWhitespace();

		if (currentIndex != 0 && token != null && token.getType().equals(TokenType.EOF)) {
			throw new LexerException("Last token has already been consumed!");
		}

		if (currentIndex == data.length) {
			token = new Token(TokenType.EOF, null);
			currentIndex++;
			return token;
		}

		if (lexerState.equals(LexerState.BASIC)) {
			if (data[currentIndex] == '#') {
				token = new Token(TokenType.SYMBOL, '#');
			} else if (data[currentIndex] == '\\') {
	
				backslashCheck();
	
				currentIndex++;
				token = new Token(TokenType.WORD, getWord());
			} else if (Character.isLetter(data[currentIndex])) {
				token = new Token(TokenType.WORD, getWord());
			} else if (Character.isDigit(data[currentIndex])) {
				token = new Token(TokenType.NUMBER, getNumber());
			} else {
				token = new Token(TokenType.SYMBOL, getSymbol());
			}
		} else {
			if (data[currentIndex] == '#') {
				token = new Token(TokenType.SYMBOL, '#');
			} else {
				token = new Token(TokenType.WORD, getWordExtended());
			} 
		}

		currentIndex++;
		return token;
	}

	/**
	 * Method getting symbol character from the character array.
	 * 
	 * @return symbol character from the character array at current index
	 */
	private Character getSymbol() {
		return data[currentIndex];
	}

	/**
	 * Method getting currently analyzed token.
	 * 
	 * @return currently analyzed token
	 */
	public Token getToken() {
		return token;
	}

	/**
	 * Method for jumping over the whitespace while changing current index lexer is positioned at in the data array.
	 */
	private void jumpOverWhitespace() {
		if (data.length == 0) {
			return;
		}

		for (int i = currentIndex, len = data.length; currentIndex < len; i++) {
			if (!Character.isWhitespace(data[currentIndex])) {
				break;
			}
			currentIndex++;
		}
	}
	
	/**
	 * Method getting String in extended mode of lexer work which is marked by "#" sign.
	 * 
	 * @return String word built in extended lexer mode
	 */
	private String getWordExtended() {
		String word = "";

		char currentData = data[currentIndex];
		while (!Character.isWhitespace(currentData)) {
			word += currentData;

			if (currentIndex + 1 == data.length || data[currentIndex + 1] == '#') {
				break;
			}
			currentIndex++;
			currentData = data[currentIndex];
		}
		return word;
	}

	/**
	 * Method getting String in basic (default) mode of lexer work.
	 * 
	 * @return String word built in basic lexer mode
	 */
	private String getWord() {
		String word = "";

		char currentData = data[currentIndex];
		while (!Character.isWhitespace(currentData)) {
			if (currentData == '\\') {
				backslashCheck();
				currentIndex++;
				currentData = data[currentIndex];
				if (Character.isDigit(currentData) && Character.isDigit(data[currentIndex + 1])) {
					word += currentData;
					break;
				}
			}

			word += currentData;

			if (currentIndex + 1 == data.length
					|| !(Character.isLetter(data[currentIndex + 1]) || data[currentIndex + 1] == '\\')) {
				break;
			}
			currentIndex++;
			currentData = data[currentIndex];
		}
		return word;
	}

	/**
	 * Method getting numerical value of Long type beginning from current index.
	 * 
	 * @return numerical value of Long type as a token produced by a lexer
	 * @throws LexerException if number formatting was unsuccessful
	 */
	private Long getNumber() {
		String numStr = "";

		char currentData = data[currentIndex];
		while (!Character.isWhitespace(currentData)) {
			if (currentData == '\\') {
				backslashCheck();
				currentIndex++;
				currentData = data[currentIndex];
			}

			if (!Character.isDigit(currentData)) {
				break;
			}

			numStr += currentData;
			if (currentIndex + 1 == data.length || !Character.isDigit(data[currentIndex + 1])) {
				break;
			}
			currentIndex++;
			currentData = data[currentIndex];
		}

		try {
			return Long.parseLong(numStr);
		} catch (NumberFormatException ex) {
			throw new LexerException("Number cannot be presented as a Long!");
		}
	}

	/**
	 * Method checking if there is a character after the backslash at current index.
	 * 
	 * @throws LexerException if there is no more token to consume or the character being escaped is not a digit or backslash (which are only types allowed for escaping)
	 */
	private void backslashCheck() {
		if (currentIndex + 1 >= data.length) {
			throw new LexerException("There is no token to consume!");
		}
		char c = data[currentIndex + 1];
		if (!Character.isDigit(c) && c != '\\') {
			throw new LexerException("Cannot escape character which is not a digit or backslash!");
		}
	}

}
