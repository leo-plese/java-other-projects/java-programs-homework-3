package hr.fer.zemris.java.hw03.prob1;

/**
 * Class representing exception thrown by lexer.
 * 
 * @author Leo
 *
 */
public class LexerException extends RuntimeException {

	/**
	 * version number - identifier of this class
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * default constructor calling <code>RuntimeException</code> super constructor
	 */
	public LexerException() {
		super();
	}

	/**
	 * constructor given information message calling <code>RuntimeException</code> super constructor forwarding message to it
	 */
	public LexerException(String message) {
		super(message);
	}
}
