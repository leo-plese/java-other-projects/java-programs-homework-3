package hr.fer.zemris.java.hw03.prob1;

/**
 * Enum listing possible lexer states.
 * 
 * @author Leo
 *
 */
public enum LexerState {
	/**
	 * enum value representing basic lexer work mode
	 */
	BASIC,
	/**
	 * enum value representing extended lexer work mode
	 */
	EXTENDED;
}
