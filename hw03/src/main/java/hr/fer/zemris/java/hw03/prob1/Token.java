package hr.fer.zemris.java.hw03.prob1;

/**
 * Class representing model of a token given by lexer.
 * 
 * @author Leo
 *
 */
public class Token {
	
	/**
	 * type of token (enlisted in the class in this package)
	 */
	private TokenType type;
	/**
	 * token value of any type (object type)
	 */
	private Object value;
	
	/**
	 * constructor initializing a token given its type and value
	 * 
	 * @param type token type
	 * @param value token object value
	 */
	public Token(TokenType type, Object value) {
		super();
		this.type = type;
		this.value = value;
	}

	/**
	 * Method getting token type.
	 * 
	 * @return token type
	 */
	public TokenType getType() {
		return type;
	}

	/**
	 * Method getting token value.
	 * 
	 * @return token value
	 */
	public Object getValue() {
		return value;
	}

}
