package hr.fer.zemris.java.hw03.prob1;

/**
 * Enum listing token types.
 * 
 * @author Leo
 *
 */
public enum TokenType {
	/**
	 * token type representing end of file
	 */
	EOF,
	/**
	 * token type representing word element
	 */
	WORD,
	/**
	 * token type representing number element
	 */
	NUMBER,
	/**
	 * token type representing symbol element
	 */
	SYMBOL;
}
