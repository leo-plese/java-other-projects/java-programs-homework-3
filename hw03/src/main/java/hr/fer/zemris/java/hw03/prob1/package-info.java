/**
 * Package containing classes pertaining to <code>Lexer</code> class and classes associated with it.
 * 
 * @author Leo
 *
 */
package hr.fer.zemris.java.hw03.prob1;