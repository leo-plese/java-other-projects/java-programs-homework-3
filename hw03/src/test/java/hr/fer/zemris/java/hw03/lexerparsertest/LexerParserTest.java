package hr.fer.zemris.java.hw03.lexerparsertest;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;
import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.Node;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;

public class LexerParserTest {

	@Test
	public void testOKInputFromFile() {
		String docBody1 = loader("docTest1.txt");
		String docBody2 = loader("docTest2.txt");
		String docBody3 = loader("docTest3.txt");
		String[] docBodies = { docBody1, docBody2, docBody3 };

		for (String docBody : docBodies) {

			SmartScriptParser parser = null;
			try {
				parser = new SmartScriptParser(docBody);
			} catch (SmartScriptParserException e) {
				System.out.println("Unable to parse document!");
				System.exit(-1);
			} catch (Exception e) {
				System.out.println("If this line ever executes, you have failed this class!");
				System.exit(-1);
			}
			
			DocumentNode document = parser.getDocumentNode();
			String originalDocumentBody = createOriginalDocumentBody(document);
			System.out.println("ORIGINAL");
			System.out.println(originalDocumentBody); // should write something like original
			System.out.println("ORIGINALend");
			System.out.println(">" + docBody);
			System.out.println(">" + originalDocumentBody);
			// content of docBody

			SmartScriptParser parser2 = new SmartScriptParser(originalDocumentBody);
			DocumentNode document2 = parser2.getDocumentNode();
			String originalDocumentBody2 = createOriginalDocumentBody(document2);
			System.out.println("ORIGINAL_222");
			System.out.println(originalDocumentBody2); // should write something like original
			System.out.println("ORIGINALend_222");
			// now document and document2 should be structurally identical trees

			assertEquals(document.numberOfChildren(), document2.numberOfChildren());
		}
	}

	@Test
	public void testNotOKInputFromFile() {

		String docBody1 = loader("docTest5.txt");
		String docBody2 = loader("docTest6.txt");
		String docBody3 = loader("docTest7.txt");
		String docBody4 = loader("docTest8.txt");
		String docBody5 = loader("docTest9.txt");
		String docBody6 = loader("docTest10.txt");

		String[] docBodies = { docBody1, docBody2, docBody3, docBody4, docBody5, docBody6 };

		for (String docBody : docBodies) {

			SmartScriptParser parser = null;
			assertThrows(SmartScriptParserException.class, () -> new SmartScriptParser(docBody));
		}

	}

	private static String createOriginalDocumentBody(DocumentNode document) {
		String original = "";
		System.out.println("documentNodett " + document);
		System.out.println("children " + document.numberOfChildren());
		for (int i = 0, n = document.numberOfChildren(); i < n; i++) {
			Node child = document.getChild(i);
			System.out.println("DOCchild " + child);
			if (child instanceof TextNode) {
				String txt = ((TextNode) child).getText();
				original += txt;

				System.out.println(original);
			} else if (child instanceof EchoNode) {
				Element[] echoElems = ((EchoNode) child).getElements();
				original += "{$ ";
				for (Element el : echoElems) {
					original += el.asText() + " ";
				}
				original += " $}";

				System.out.println(original);
			} else if (child instanceof ForLoopNode) {
				ForLoopNode forNode = (ForLoopNode) child;
				ElementVariable var = forNode.getVariable();
				Element start = forNode.getStartExpression();
				Element end = forNode.getEndExpression();
				Element step = forNode.getStepExpression();

				original += "{$ FOR ";
				String stepStr = step == null ? "" : " " + step.asText();
				original += " " + var.asText() + " " + start.asText() + " " + end.asText() + stepStr;
				original += " $}";

				System.out.println(original);

				for (int j = 0, f = forNode.numberOfChildren(); j < f; j++) {

					Node childFor = forNode.getChild(j);
					System.out.println("FORchild " + childFor);
					if (childFor instanceof TextNode) {
						String txt = ((TextNode) childFor).getText();
						original += txt;
					} else if (childFor instanceof EchoNode) {
						Element[] echoElems = ((EchoNode) childFor).getElements();
						original += "{$";
						for (Element el : echoElems) {
							original += el.asText() + " ";
						}
						original += "$}";
					}
					System.out.println(original);
				}
				original += "{$END$}";
			} else {
				throw new SmartScriptParserException("Unexisting node!");
			}
		}

		return original;
	}

	private String loader(String filename) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try (InputStream is = this.getClass().getClassLoader().getResourceAsStream(filename)) {
			byte[] buffer = new byte[1024];
			while (true) {
				int read = is.read(buffer);
				if (read < 1)
					break;
				bos.write(buffer, 0, read);
			}
			return new String(bos.toByteArray(), StandardCharsets.UTF_8);
		} catch (IOException ex) {
			return null;
		}
	}

}
